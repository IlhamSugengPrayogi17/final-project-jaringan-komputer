/*
    - Masih memperbaiki sambungan antar player yang masuk
    - random soal
    - Broadcast pemenang
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

struct quest
{
    public string question;
    public string answer;
}

namespace sv
{
    class Program
    {
        static readonly object _locked = new object();
        static readonly Dictionary<int, TcpClient> listClients = new Dictionary<int, TcpClient>();

        static quest[] quiz = new quest[30];

        public static void Main()
        {
            quiz[0].question = " Tomb rider adalah salah satu game yang terinspirasi oleh film dengan judul yang sama";
            quiz[1].question = " Desain Karakter pada seri Dragon Quest dtangani oleh Akira Toriyama";
            quiz[2].question = " The Last of Us adalah game dengan genre Action-Survival Horror";
            quiz[3].question = " Playstation 3 rilis untuk pertama kali pada tahun 2007";
            quiz[4].question = " Developer dari seri Dark Souls adalah Activision";
            quiz[5].question = " Pemenang GOTY pada tahun 2018 adalah God of War";
            quiz[6].question = " Resident Evil adalah sebuah franchise game yang dibuat oleh Ubisoft";
            quiz[7].question = " Grand Theft Auto: Vice City adalah game Rockstar yang dirilis di konsol PS2";
            quiz[8].question = " Karakter utama dari game Final Fantasy 9 adalah Squall Leonhart";
            quiz[9].question = " Developer Pencipta seri The Sims adalah Maxis Software";
            quiz[10].question = " Nama dari Remake Crash Bandicoot untuk console next-gen adalah Insane Trylogi";
            quiz[11].question = " Karakter utama pada game Grand Theft Auto 3 bernama Nico Belic";
            quiz[12].question = " Nama geng utama dari karakter utama GTA San Andreas adalah Grove Street";
            quiz[13].question = " Frank Tempenny merupakan salah satu protagonis pada game GTA San Andreas";
            quiz[14].question = " Nama gunung tertinggi yang ada di Grand Theft Auto 5 adalah Mount Lemming";
            quiz[15].question = " Shigeru Miyamoto adalah pencipta dari Super Mario Bros";
            quiz[16].question = " Nama dari bos utama pada seri Super Mario adalah Yoshi";
            quiz[17].question = " Nama dari game kolaborasi antara Super Mario dan Rabbids adalah Mario Vs Rabbids";
            quiz[18].question = " Super mario Galaxy adalah salah satu game super mario untuk Nintendo Switch";
            quiz[19].question = " Pada game Super Mario Bros original player juga bisa menggunakan kontroler 2 untuk menggerakkan Luigi";
            quiz[20].question = " Joker dari Persona 5 adalah salah satu karakter yang bisa dimainkan di game Super Smash Bros";
            quiz[21].question = " Sutradara dari Super Smash Bros Series bernama Masahiro Sakurai";
            quiz[22].question = " Super Smash Bros Brawl adalah judul dari seri Smash yang rilis di Wii U";
            quiz[23].question = " Super Smash Bros pertama kali rilis pada tahun 1999 di platform Nintendo 64";
            quiz[24].question = " Karakter utama pada mode Story di Super Smash Bros Ultimate adalah Mario";
            quiz[25].question = " Santa Monica Studio adalah developer yang membuat seri game The Witcher";
            quiz[26].question = " Breath of the Wild adalah seri Zelda yang rilis untuk console Nintendo switch dan Wii U";
            quiz[27].question = " Hideo Kojima adalah orang yang berperan besar untuk kesuksesan seri Metal Gear";
            quiz[28].question = " Respawn adalah developer yang membuat game battleroyal yaitu Apex Legend";
            quiz[29].question = " Uncaharted memiliki karakter utama bernama Nathan Drake";

            quiz[0].answer = "false";
            quiz[1].answer = "true";
            quiz[2].answer = "true";
            quiz[3].answer = "false";
            quiz[4].answer = "false";
            quiz[5].answer = "true";
            quiz[6].answer = "false";
            quiz[7].answer = "true";
            quiz[8].answer = "false";
            quiz[9].answer = "true";
            quiz[10].answer = "true";
            quiz[11].answer = "false";
            quiz[12].answer = "true";
            quiz[13].answer = "false";
            quiz[14].answer = "false";
            quiz[15].answer = "true";
            quiz[16].answer = "false";
            quiz[17].answer = "false";
            quiz[18].answer = "false";
            quiz[19].answer = "false";
            quiz[20].answer = "true";
            quiz[21].answer = "true";
            quiz[22].answer = "false";
            quiz[23].answer = "true";
            quiz[24].answer = "false";
            quiz[25].answer = "false";
            quiz[26].answer = "true";
            quiz[27].answer = "true";
            quiz[28].answer = "true";
            quiz[29].answer = "true";

            int count = 1;
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("Server Started . . .");
                while (true)
                {
                    Console.WriteLine("Waiting . . .");

                    TcpClient client = listener.AcceptTcpClient();
                    lock (_locked) listClients.Add(count, client);
                    Console.WriteLine("Accepted" + " Client no: " + Convert.ToString(count));

                    Thread t = new Thread(Game);
                    t.Start(count);
                    count++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }


        private static void Game(object argument)
        {
            int id = (int)argument;
            TcpClient client;

            lock (_locked) client = listClients[id];
            try
            {
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                string name = reader.ReadLine();
                Console.WriteLine("Client Name : {0}", name);
                string masuk = reader.ReadLine();
                int score = 0;

                Console.WriteLine(masuk);

                while (true)
                {
                    if (masuk == "1")
                    {
                        int soalAcak;

                        Random random = new Random();
                        soalAcak = random.Next(0, 29);

                        writer.WriteLine(quiz[soalAcak].question);
                        writer.Flush();

                        string jawaban = reader.ReadLine();
                        Console.WriteLine(jawaban);

                        if (jawaban == quiz[soalAcak].answer)
                        {
                            score++;
                            Console.WriteLine("Jawaban Benar");
                            writer.WriteLine(score);
                            writer.Flush();
                            if (score == 7)
                            {
                                broadcast(name + " Win\n");

                                writer.WriteLine("quit");
                                writer.Flush();
                                client.Close();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Jawaban Salah");
                            writer.WriteLine(score);
                            writer.Flush();
                        }

                    }
                    else if (masuk == "2")
                    {
                        client.Close();
                    }
                                     
                }
                reader.Close();
                writer.Close();
                client.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("Client keluar");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }

        public static void broadcast(string data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

            lock (_locked)
            {
                foreach (TcpClient client in listClients.Values)

                {
                    NetworkStream stream = client.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
    }
}