/*
    - Masih memperbaiki sambungan
    - Penambahan tampilan
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

public class Client
{
    public static void Main(string[] args)
    {
        try
        {
            string pertanyaan = " ";
            string jawaban = " ";
            string score = " ";

            TcpClient client = new TcpClient("127.0.0.1", 8080);
            String i = String.Empty;

            Thread thread = new Thread(o => Receive((TcpClient)o));

            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());

            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("           ___________________________________________________            ");
            Console.WriteLine("          |                                                   |           ");
            Console.WriteLine("          |      Salutations! Brave hero of the class!        |           ");
            Console.WriteLine("          |           Welcome to the legendary quiz           |           ");
            Console.WriteLine("          |              of Gaming World!!!                   |           ");
            Console.WriteLine("          |                                                   |           ");
            Console.WriteLine("          |___________________________________________________|           ");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("First you must enter you name.");

            Console.Write("\nYour Name : ");
            i = Console.ReadLine();
            writer.WriteLine(i);
            writer.Flush();

            Console.WriteLine("This is True or False Game");
            Console.WriteLine("You must get 7 point first if you want to have the crown");
            Console.WriteLine("1. Play Now");
            Console.WriteLine("2. Quit");

            Console.Write("\n Choose : ");
            i = Console.ReadLine();
            writer.WriteLine(i);
            writer.Flush();

            while (true)
            {
                Console.Write("\n");
                String server_string = " ";

                pertanyaan = reader.ReadLine();
                Console.WriteLine(pertanyaan);
                Console.WriteLine();

                jawaban = Console.ReadLine();
                writer.WriteLine(jawaban);
                writer.Flush();

                score = reader.ReadLine();
                Console.WriteLine(score);

                if (score == "7")
                {
                    server_string = reader.ReadLine();
                    Console.WriteLine(server_string);
                    reader.Close();
                    writer.Close();
                    client.Close();
                }
            }
            thread.Join();
            reader.Close();
            writer.Close();
            client.Close();
            Console.ReadKey();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    static void Receive(TcpClient client)
    {
        NetworkStream netStr = client.GetStream();
        byte[] receivedBytes = new byte[1024];
        int byteCount;

        while ((byteCount = netStr.Read(receivedBytes, 0, receivedBytes.Length)) > 0)
        {
            Console.Write(Encoding.ASCII.GetString(receivedBytes, 0, byteCount));
        }
    }
}

